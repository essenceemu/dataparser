package org.dataparser.provider.dat.ItemName;

import java.lang.reflect.Field;

public class ItemNameData {
    public String id;
    public String name;
    public String additionalname;
    public String description;
    public String popup;
    public String default_action;
    public String use_order;
    public String name_class;
    public String color;
    public String Tooltip_Texture;
    public String is_trade;
    public String is_drop;
    public String is_destruct;
    public String is_private_store;
    public String keep_type;
    public String is_npctrade;
    public String is_commission_store;

    public String getId() {
        return id;
    }
}
