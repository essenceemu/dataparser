package org.dataparser.template;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class Environment {
    private Map<String, String> variables = new HashMap<>();

    public Environment() {
    }

    public Map<String, String> getVariables() {
        return variables;
    }

    public String getVariableValue(String name) {
        return variables.get(name);
    }

    public void setVariable(String name, String value) {
        variables.put(name, value);
    }

    public void setVariablesFromClassFields(Object object, String prefix) {
        Field[] fields = object.getClass().getFields();
        for (Field field : fields) {
            if (Modifier.isPublic(field.getModifiers())) {
                try {
                    String variableName = "%" + prefix + object.getClass().getSimpleName() + "." + field.getName() + "%";
                    String value = (String)field.get(object);
                    setVariable(variableName, value);
                }
                catch (IllegalAccessException e) {
                    System.out.println(e.toString());
                }
            }
        }
    }
}
