package org.dataparser.provider.l2central.npc;

import org.dataparser.provider.dat.ClientDatProvider;
import org.dataparser.provider.dat.npcname.NpcNameData;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class NpcDataParser {
    static String baseUrl = "https://l2central.info/classic/";
    private List<NpcBasicInfo> npcBasicInfoList = new ArrayList<>();

    public List<NpcBasicInfo> getNpcBasicInfo() {
        return npcBasicInfoList;
    }

    public void load() {
        Collection<NpcNameData> nameDataList = ClientDatProvider.getInstance().getNpcNames();
        for (NpcNameData nameData : nameDataList) {
            NpcBasicInfo info = loadNpc(baseUrl + nameData.name.replace(" ", "_").replace("[", "").replace("]", ""));
            //NpcBasicInfo info = loadNpc("https://l2central.info/classic/Баньши");
            //NpcBasicInfo info = loadNpc("https://l2central.info/classic/%D0%93%D0%BB%D0%B0%D0%B7_%D0%A1%D0%BF%D0%B0%D1%81%D0%B8%D1%82%D0%B5%D0%BB%D1%8F");
            //NpcBasicInfo info = loadNpc("https://l2central.info/classic/%D0%9A%D0%BE%D1%80%D0%BE%D0%BB%D1%8C_%D0%A2%D0%B0%D0%B9_%D0%A4%D0%B5%D1%80%D0%BE%D0%BD");

            if (info == null) {
                continue;
            }
            info.id = nameData.id;
            npcBasicInfoList.add(info);
        }

        dumpAllNpcInfoToFileIfNeeded();
    }

    private NpcBasicInfo loadNpc(String url) {
        try {
            Document doc = Jsoup.connect(url).get();
            Elements isMobElements = doc.select("#Трофеи");
            if (!isMobElements.isEmpty()) {
                Elements npcBriefInfo2Elements = doc.select("#npc_brief_info_2");
                Elements npcBriefInfo4Elements = doc.select("#npc_brief_info_4");
                if (npcBriefInfo2Elements.size() == 0 && npcBriefInfo4Elements.size() == 0) {
                    System.out.println("Can't find npc bried info for url " + url);
                }

                boolean isRaidBossInfo = (npcBriefInfo4Elements.size() > 0);
                if (isRaidBossInfo) {
                    return parseRaidboss(doc);
                }
                else {
                    return parseMonster(doc);
                }
            }
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }

        return null;
    }

    private NpcBasicInfo parseMonster(Document doc) {
        NpcBasicInfo info = new NpcBasicInfo();

        Elements infoTables = doc.select(".quests");
        if (infoTables.size() != 5) {
            return null;
        }

        Element npcStatsTable = infoTables.get(1);
        if (!parseNpcInfo(npcStatsTable, info)) {
            return null;
        }

        info.skills = parseNpcSkills(npcStatsTable);

        Element generalItemTable = infoTables.get(2);
        if (generalItemTable != null) {
            info.generalItems = parseDropTable(generalItemTable);
        }

        Element otherItemsTable = infoTables.get(3);
        if (otherItemsTable != null) {
            info.otherItems = parseDropTable(otherItemsTable);
        }
        Element spoilItemsTable = infoTables.get(4);
        if (spoilItemsTable != null) {
            info.spoilItems = parseDropTable(spoilItemsTable);
        }

        return info;
    }

    private NpcBasicInfo parseRaidboss(Document doc) {
        NpcBasicInfo info = new NpcBasicInfo();

        Elements infoTables = doc.select(".quests");
        if (infoTables.size() != 5) {
            return null;
        }

        Element npcStatsTable = infoTables.get(2);
        if (!parseNpcInfo(npcStatsTable, info)) {
            return null;
        }

        info.skills = parseNpcSkills(npcStatsTable);

        Element generalItemTable = infoTables.get(4);
        if (generalItemTable != null) {
            info.generalItems = parseDropTable(generalItemTable);
        }

        Elements tableRows = generalItemTable.select("tbody > tr");
        Elements tableHeaders = generalItemTable.select("tbody > tr > th");
        if (tableHeaders.size() == 6) {
            int indexOfOtherItemRow = 0;
            for (; indexOfOtherItemRow < tableRows.size(); ++indexOfOtherItemRow) {
                if (tableRows.get(indexOfOtherItemRow) == tableHeaders.get(3).parent()) {
                    break;
                }
            }

            if (indexOfOtherItemRow < tableRows.size()) {
                info.otherItems = new ArrayList<>();
                int sizeOfAllItem = info.generalItems.size();
                for (int i = sizeOfAllItem; i >= indexOfOtherItemRow; --i) {
                    info.otherItems.add(info.generalItems.get(i - 1));
                    info.generalItems.remove(i - 1);
                }
            }
        }

        return info;
    }

    private boolean parseNpcInfo(Element npcStatsTable, NpcBasicInfo npcInfo) {
        Elements infoRowsElements = npcStatsTable.select("tbody > tr");
        for (Element infoRow : infoRowsElements) {
            Elements statRowElements = infoRow.select("td");
            if (statRowElements.size() != 2) {
                continue;
            }

            String statName = statRowElements.get(0).text();
            switch (statName) {
                case "Ур.": {
                    npcInfo.level = statRowElements.get(1).text();
                    break;
                }
                case "HP": {
                    npcInfo.hp = statRowElements.get(1).text().replace(",", "");
                    break;
                }
                case "MP": {
                    npcInfo.mp = statRowElements.get(1).text().replace(",", "");
                    break;
                }
                case "EXP": {
                    npcInfo.exp = statRowElements.get(1).text().replace(",", "");
                    break;
                }
                case "Стихия": {
                    if (npcInfo.attInfo == null) {
                        npcInfo.attInfo = new NpcAttributeInfo();
                    }
                    npcInfo.attInfo.attr = NpcAttributeInfo.Attribute.valueFromL2Central(statRowElements.get(1).text());
                    break;
                }
                case "EXP Стихии": {
                    if (npcInfo.attInfo == null) {
                        npcInfo.attInfo = new NpcAttributeInfo();
                    }
                    npcInfo.attInfo.exp = statRowElements.get(1).text().replace(",", "");
                    break;
                }
                case "SP": {
                    npcInfo.sp = statRowElements.get(1).text().replace(",", "");
                    break;
                }
                case "RP": {
                    npcInfo.rp = statRowElements.get(1).text().replace(",", "");
                    break;
                }
            }
        }

        return true;
    }

    private List<NpcSkillInfo> parseNpcSkills(Element npcStatsTable) {
        Elements skillInfoElements = npcStatsTable.select("tbody > tr:nth-child(10) > td > a");
        if (skillInfoElements.size() == 0) {
            return null;
        }

        List<NpcSkillInfo> skillInfoList = new ArrayList<>();
        for (Element skillInfoElement : skillInfoElements) {
            String[] hrefParts = skillInfoElement.attr("href").split(":");
            if (hrefParts.length != 2) {
                continue;
            }
            String[] imgParts = hrefParts[1].split("_");
            if (imgParts.length != 3) {
                continue;
            }

            NpcSkillInfo skillInfo = new NpcSkillInfo();
            skillInfo.id = imgParts[1];
            skillInfo.level = imgParts[2].replace(".jpg", "");

            skillInfoList.add(skillInfo);
        }

        return skillInfoList;
    }

    private static List<NpcItemDropInfo> parseDropTable(Element dropTable) {
        List<NpcItemDropInfo> dropInfoList = new ArrayList();

        Elements dropItemElements =  dropTable.select("tbody > tr");
        if (dropItemElements != null) {
            for (Element itemElement : dropItemElements) {
                String itemName = itemElement.select("td:nth-child(1) > a.image > img").attr("alt");
                String amount = itemElement.select("td:nth-child(2)").text();
                String chance = itemElement.select("td:nth-child(3)").text();
                if (itemName.length() == 0 || amount.length() == 0 || chance.length() == 0) {
                    continue;
                }

                NpcItemDropInfo info = new NpcItemDropInfo();
                info.id = itemName.split(" ")[1].replace(".jpg", "").trim();
                if (amount.contains("—")) {
                    String[] amountParts = amount.split("—");
                    info.minAmount = amountParts[0].trim();
                    info.maxAmount = amountParts[1].trim();
                }
                else {
                    info.minAmount = amount;
                    info.maxAmount = amount;
                }

                if (chance.contains("—")) {
                    String[] chanceParts = chance.split("—");
                    info.minChance = chanceParts[0].trim();
                    info.maxChance = chanceParts[1].trim();
                }
                else {
                    chance = chance.replace("~ ", "");
                    info.minChance = chance;
                    info.maxChance = chance;
                }

                dropInfoList.add(info);
            }
        }

        return dropInfoList;
    }

    private void dumpAllNpcInfoToFileIfNeeded() {
        try {
            File newDataFile = new File("data/l2central/NpcInfoData.txt");
            if (newDataFile.exists()) {
                return;
            }

            newDataFile.delete();
            newDataFile.createNewFile();
            FileWriter fw = new FileWriter(newDataFile);

            StringBuilder builder = new StringBuilder(1024);
            for (NpcBasicInfo npcInfo : npcBasicInfoList) {
                String prefix = "npcinfo_begin\tid=" + npcInfo.id +
                        "\tlevel=" + npcInfo.level +
                        "\thp=" + npcInfo.hp +
                        "\tmp=" + npcInfo.mp +
                        "\texp=" + npcInfo.exp +
                        "\tsp=" + npcInfo.sp +
                        "\trp=" + npcInfo.rp;
                builder.append(prefix);

                if (npcInfo.attInfo != null) {
                    builder.append("\tattr=[").append(npcInfo.attInfo.attr.toString()).append(";").append(npcInfo.attInfo.exp).append("]");
                }

                if (npcInfo.skills != null && npcInfo.skills.size() > 0) {
                    builder.append("\tskills={");
                    for (NpcSkillInfo skillInfo : npcInfo.skills) {
                        builder.append("[").append(skillInfo.id).append(";").append(skillInfo.level).append("];");
                    }
                    builder.append("}");
                }

                if (npcInfo.generalItems != null && npcInfo.generalItems.size() > 0) {
                    builder.append("\tgeneral_drop={");
                    for (NpcItemDropInfo dropInfo : npcInfo.generalItems) {
                        builder.append("[").append(dropInfo.id).append(";").append(dropInfo.minAmount).append(";").append(dropInfo.maxAmount).append(";").append(dropInfo.minChance).append(";").append(dropInfo.maxChance).append("];");
                    }
                    builder.append("}");
                }

                if (npcInfo.otherItems != null && npcInfo.otherItems.size() > 0) {
                    builder.append("\tother_drop={");
                    for (NpcItemDropInfo dropInfo : npcInfo.otherItems) {
                        builder.append("[").append(dropInfo.id).append(";").append(dropInfo.minAmount).append(";").append(dropInfo.maxAmount).append(";").append(dropInfo.minChance).append(";").append(dropInfo.maxChance).append("];");
                    }
                    builder.append("}");
                }

                if (npcInfo.spoilItems != null && npcInfo.spoilItems.size() > 0) {
                    builder.append("\tspoil_drop={");
                    for (NpcItemDropInfo dropInfo : npcInfo.spoilItems) {
                        builder.append("[").append(dropInfo.id).append(";").append(dropInfo.minAmount).append(";").append(dropInfo.maxAmount).append(";").append(dropInfo.minChance).append(";").append(dropInfo.maxChance).append("];");
                    }
                    builder.append("}");
                }

                builder.append("\tnpcinfo_end\n");
                fw.write(builder.toString());
                fw.flush();
                builder.setLength(0);
            }
            fw.close();

        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
}
