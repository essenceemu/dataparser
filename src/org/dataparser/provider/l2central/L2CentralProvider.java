package org.dataparser.provider.l2central;

import org.dataparser.provider.l2central.classes.ClassInfo;
import org.dataparser.provider.l2central.classes.ClassesSkillsParser;
import org.dataparser.provider.l2central.classes.RaceInfo;
import org.dataparser.provider.l2central.npc.CachedCentralNpcInfoParser;
import org.dataparser.provider.l2central.npc.NpcBasicInfo;
import org.dataparser.provider.l2central.npc.NpcDataParser;
import org.dataparser.provider.l2central.skillinfo.CachedCentralSkillInfoData;
import org.dataparser.provider.l2central.skillinfo.CachedCentralSkillInfoParser;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class L2CentralProvider {
    ClassesSkillsParser skillsParser;
    NpcDataParser npcDataParser;

    private Map<String, CachedCentralSkillInfoData> cachedSkillInfoDataMap = new HashMap<>();
    private Map<String, NpcBasicInfo> cachedNpcInfoDataMap = new HashMap<>();

    public L2CentralProvider() {
        if (CachedCentralNpcInfoParser.hasCachedData()) {
            cachedNpcInfoDataMap = new CachedCentralNpcInfoParser().parse().stream().collect(Collectors.toMap(NpcBasicInfo::getId, info -> info, new BinaryOperator<NpcBasicInfo>() {
                @Override
                public NpcBasicInfo apply(NpcBasicInfo npcBasicInfo, NpcBasicInfo npcBasicInfo2) {
                    return npcBasicInfo;
                }
            }, HashMap::new));
        }
        else {
            npcDataParser = new NpcDataParser();
            npcDataParser.load();
        }

        skillsParser = new ClassesSkillsParser();
        skillsParser.load();

        if (CachedCentralSkillInfoParser.hasCachedData()) {
            cachedSkillInfoDataMap = new CachedCentralSkillInfoParser().parse().stream().collect(Collectors.toMap(CachedCentralSkillInfoData::getSkillId, skill -> skill, new BinaryOperator<CachedCentralSkillInfoData>() {
                @Override
                public CachedCentralSkillInfoData apply(CachedCentralSkillInfoData cachedSkillInfoData, CachedCentralSkillInfoData cachedSkillInfoData2) {
                    return cachedSkillInfoData;
                }
            }, HashMap::new));
        }
    }

    public RaceInfo getRace(String name) {
        return skillsParser.getRace(name);
    }

    public Collection<RaceInfo> getRaceInfos() {
        return skillsParser.getRaceInfoList();
    }

    public ClassInfo getOrLoadClassInfo(ClassInfo classInfo) {
        return skillsParser.getOrLoadClassInfo(classInfo);
    }

    public CachedCentralSkillInfoData getCachedSkillInfo(String id) {
        return cachedSkillInfoDataMap.get(id);
    }

    public Collection<CachedCentralSkillInfoData> getCachedSkillInfos() {
        return cachedSkillInfoDataMap.values();
    }

    public static L2CentralProvider getInstance() {
        return Singleton.INSTANCE;
    }

    private static class Singleton {
        private static final L2CentralProvider INSTANCE = new L2CentralProvider();
    }
}
