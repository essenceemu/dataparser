package org.dataparser.provider.dat.npcgrp;

public class NpcGrpData {
    public String npc_id;
    public String class_name;
    public String npc_speed;
    public String social;
    public String hpshowable;
    public String collision_radius;
    public String collision_radius_2;
    public String collision_height;
    public String collision_height_2;
    public String slot_lhand;
    public String slot_rhand;
    public String slot_chest;
    public String org_hp;
    public String org_mp;

    public String getId() {
        return npc_id;
    }
}
