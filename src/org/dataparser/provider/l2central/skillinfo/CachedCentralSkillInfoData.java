package org.dataparser.provider.l2central.skillinfo;

public class CachedCentralSkillInfoData {
    public String race_name;
    public String class_name;
    public String id;
    public String icon;
    public String name;
    public String magicLevel;
    public String level;
    public String type;
    public String mp;
    public String hp;
    public String spCost;
    public String range;
    public String attr;
    public String description;

    String cachedSkillId;

    public String getSkillId() {
        if (cachedSkillId == null) {
            cachedSkillId = id + ":" + level + ":0";
        }
        return cachedSkillId;
    }
}
