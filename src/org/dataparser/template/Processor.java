package org.dataparser.template;

import org.dataparser.template.tokens.Block;
import org.dataparser.template.tokens.Token;
import org.dataparser.template.tokens.Variable;

import java.util.ArrayList;
import java.util.List;

public class Processor {
    public static List<Token> getTemplateTokens(Template template) {
        String templateString = template.getTemplate();
        List<Token> tokens = new ArrayList<>();
        int tokenStartOffset = 0;
        boolean isVariableStarted = false;
        Block block = null;
        for (int i = 0, n = templateString.length(); i < n; ++i){
            char c = templateString.charAt(i);
            if (c == '@') {
                if (block != null) {
                    Token newToken = new Token(templateString.substring(tokenStartOffset, i));
                    block.addToken(newToken);
                    block = null;
                }
                else {
                    Token newToken = new Token(templateString.substring(tokenStartOffset, i));
                    tokens.add(newToken);

                    block = new Block(null);
                    tokens.add(block);
                }

                if (i + 1 < n) {
                    ++i;
                }
                tokenStartOffset = i;
            } else if (c == '%') {
                if (isVariableStarted) {
                    if (i + 1 < n) {
                        ++i;
                    }
                    isVariableStarted = false;

                    Token newToken = new Variable(templateString.substring(tokenStartOffset, i));
                    if (block != null) {
                        block.addToken(newToken);
                    }
                    else {
                        tokens.add(newToken);
                    }
                }
                else {
                    Token newToken = new Token(templateString.substring(tokenStartOffset, i));
                    if (block != null) {
                        block.addToken(newToken);
                    }
                    else {
                        tokens.add(newToken);
                    }

                    isVariableStarted = true;
                }

                tokenStartOffset = i;
            }
        }

        tokens.add(new Token(templateString.substring(tokenStartOffset)));

        return tokens;
    }

    public static List<Variable> getVariableTokens(List<Token> tokens) {
        List<Variable> variables = new ArrayList<>();
        for (Token token : tokens) {
            if (token.isVariable()) {
                variables.add((Variable) token);
            }
            else if (token.isBlock()) {
                for (Token blockToken : token.getChildren()) {
                    if (blockToken.isVariable()) {
                        variables.add((Variable) blockToken);
                    }
                }
            }
        }

        return variables;
    }

    public static void updateVariableTokens(List<Variable> tokens, Environment env) {
        for (Variable token : tokens) {
            String newValue = env.getVariableValue(token.getValue());
            if (newValue != null) {
                token.setVariableValue(newValue);
            }
        }
    }

    public static String generateTemplate(List<Token> tokens) {
        StringBuilder builder = new StringBuilder(1024);
        for (Token token : tokens) {
            if (token.isBlock() && !token.isEmpty()) {
                for (Token child : token.getChildren()) {
                    if (!child.isEmpty()) {
                        builder.append(child.getValue());
                    }
                }
            }
            else if (!token.isEmpty()) {
                builder.append(token.getValue());
            }
        }

        return builder.toString();
    }
}
