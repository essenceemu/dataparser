package org.dataparser;
;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;

import java.lang.reflect.Field;

public class FieldValueFactory<S,T> implements Callback<TableColumn.CellDataFeatures<S,T>, ObservableValue<T>> {
    private String field;

    public FieldValueFactory(String field) {
        this.field = field;
    }

    @Override
    public ObservableValue<T> call(TableColumn.CellDataFeatures<S, T> param) {
        try {
            Field classField = param.getValue().getClass().getField(field);
            try {
                Object value = classField.get(param.getValue());
                return new ReadOnlyObjectWrapper(value);
            } catch (IllegalAccessException e) {
                System.out.println(e.toString());
            }
        } catch (NoSuchFieldException e) {
            System.out.println(e.toString());
        }
        return null;
    }
}
