package org.dataparser.provider.dat.npcname;

public class NpcNameData {
    public String id;
    public String name;

    public String getId() {
        return id;
    }
}
