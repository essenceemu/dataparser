package org.dataparser.provider.pts.npcdata;

import org.dataparser.provider.BasicDataParser;

public class NpcDataParser extends BasicDataParser<NpcData> {
    @Override
    protected NpcData createData() {
        return new NpcData();
    }

    @Override
    protected String dataFilePath() {
        return "data/pts/npcdata.txt";
    }

    @Override
    public boolean canParse(String definition) {
        return definition.startsWith("npc_begin");
    }

    @Override
    protected boolean parseProperties(NpcData data, String[] properties) {
        data.type = properties[1];
        data.id = properties[2];
        data.name = properties[3];
        data.int_ = getProperty(properties, "int");

        return super.parseProperties(data, properties);
    }
}
