package org.dataparser.provider.l2central.npc;

import java.util.List;

public class NpcBasicInfo {
    public String id;

    public String level;
    public String hp;
    public String mp;
    public String exp;
    public String sp;
    public String rp;
    public NpcAttributeInfo attInfo;

    public List<NpcSkillInfo> skills;

    public List<NpcItemDropInfo> generalItems;
    public List<NpcItemDropInfo> otherItems;
    public List<NpcItemDropInfo> spoilItems;

    public NpcBasicInfo() {
        level = "0";
        hp = "0";
        mp = "0";
        exp = "0";
        sp = "0";
        rp = "0";
    }

    public String getId() {
        return id;
    }
}
