package org.dataparser.provider.pts.itemdata;

import org.dataparser.provider.BasicDataParser;

public class ItemDataParser extends BasicDataParser<ItemData> {
    @Override
    protected ItemData createData() {
        return new ItemData();
    }

    @Override
    protected String dataFilePath() {
        return "data/pts/itemdata.txt";
    }

    @Override
    public boolean canParse(String definition) {
        return definition.startsWith("item_begin");
    }

    @Override
    protected boolean parseProperties(ItemData data, String[] properties) {
        data.type = properties[1];
        data.id = properties[2];
        data.name = properties[3];

        return super.parseProperties(data, properties);
    }
}
