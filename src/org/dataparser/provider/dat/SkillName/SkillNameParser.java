package org.dataparser.provider.dat.SkillName;

import org.dataparser.provider.BasicDataParser;

public class SkillNameParser extends BasicDataParser<SkillNameData> {
    @Override
    protected SkillNameData createData() {
        return new SkillNameData();
    }

    @Override
    protected String dataFilePath() {
        return "data/dat/SkillName.txt";
    }

    @Override
    public boolean canParse(String definition) {
        return definition.startsWith("skill_begin");
    }
}
