package org.dataparser.provider.dat.SkillGrp;

import org.dataparser.provider.BasicDataParser;

public class SkillGrpParser extends BasicDataParser<SkillGrpData> {
    @Override
    protected SkillGrpData createData() {
        return new SkillGrpData();
    }

    @Override
    protected String dataFilePath() {
        return "data/dat/Skillgrp.txt";
    }

    @Override
    public boolean canParse(String definition) {
        return definition.startsWith("skill_begin");
    }
}
