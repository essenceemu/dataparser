package org.dataparser.provider.dat.ItemName;

import org.dataparser.provider.BasicDataParser;

public class ItemNameParser extends BasicDataParser<ItemNameData> {
    @Override
    protected ItemNameData createData() {
        return new ItemNameData();
    }

    @Override
    protected String dataFilePath() {
        return "data/dat/ItemName.txt";
    }

    @Override
    public boolean canParse(String definition) {
        return definition.startsWith("item_name_begin");
    }
}
