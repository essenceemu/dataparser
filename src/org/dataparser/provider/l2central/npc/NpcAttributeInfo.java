package org.dataparser.provider.l2central.npc;

public class NpcAttributeInfo {
    public enum Attribute {
        earth,
        wind,
        fire,
        water;

        public static Attribute valueFromL2Central(String attrName) {
            switch (attrName) {
                case "Земля" : {
                    return earth;
                }
                case "Ветер" : {
                    return wind;
                }
                case "Огонь" : {
                    return fire;
                }
                case "Вода" : {
                    return water;
                }
            }

            return null;
        }
    }

    public Attribute attr;
    public String exp;
}
