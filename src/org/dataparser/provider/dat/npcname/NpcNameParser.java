package org.dataparser.provider.dat.npcname;

import org.dataparser.provider.BasicDataParser;

public class NpcNameParser extends BasicDataParser<NpcNameData> {
    @Override
    protected NpcNameData createData() {
        return new NpcNameData();
    }

    @Override
    protected String dataFilePath() {
        return "data/dat/NpcName_ru.txt";
    }

    @Override
    public boolean canParse(String definition) {
        return definition.startsWith("npc_begin");
    }
}
