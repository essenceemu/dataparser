package org.dataparser.template.tokens;

import java.util.ArrayList;
import java.util.List;

public class Block extends Token {
    List<Token> tokens;

    public Block(String value) {
        super(value);
    }

    public void addToken(Token token) {
        if (tokens == null) {
            tokens = new ArrayList<>();
        }

        tokens.add(token);
    }

    @Override
    public boolean isBlock() {
        return true;
    }

    @Override
    public boolean isEmpty() {
        for (Token token : tokens) {
            if (token.isVariable() && !token.isEmpty())
                return false;
        }

        return true;
    }

    @Override
    public List<Token> getChildren() {
        return tokens;
    }
}
