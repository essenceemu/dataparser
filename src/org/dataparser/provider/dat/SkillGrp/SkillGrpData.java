package org.dataparser.provider.dat.SkillGrp;

public class SkillGrpData {
    public String skill_id;
    public String skill_level;
    public String skill_sublevel;
    public String operate_type;
    public String icon_hide;
    public String resist_cast;
    public String MagicType;
    public String mp_consume;
    public String cast_range;
    public String cast_style;
    public String hit_time;
    public String cool_time;
    public String reuse_delay;
    public String effect_point;
    public String is_magic;
    public String origin_skill;
    public String is_double;
    public String animation;
    public String skill_visual_effect;
    public String icon;
    public String icon_panel;
    public String debuff;
    public String icon_type;
    public String enchant_skill_level;
    public String enchant_icon;
    public String hp_consume;
    public String rumble_self;
    public String rumble_target;

    String cachedSkillId;

    public String getSkillId() {
        if (cachedSkillId == null) {
            cachedSkillId = skill_id + ":" + skill_level + ":" + skill_sublevel;
        }
        return cachedSkillId;
    }
}
