package org.dataparser.provider.l2central.skillinfo;

import org.dataparser.provider.BasicDataParser;

import java.nio.file.Files;
import java.nio.file.Paths;

public class CachedCentralSkillInfoParser extends BasicDataParser<CachedCentralSkillInfoData> {
    static String dataFilePath = "data/l2central/SkillInfoData.txt";
    @Override
    protected CachedCentralSkillInfoData createData() {
        return new CachedCentralSkillInfoData();
    }

    @Override
    protected String dataFilePath() {
        return dataFilePath;
    }

    @Override
    public boolean canParse(String definition) {
        return definition.startsWith("skillinfo_begin");
    }

    public static boolean hasCachedData() {
        return Files.exists(Paths.get(dataFilePath));
    }
}
