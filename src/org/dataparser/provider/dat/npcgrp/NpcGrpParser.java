package org.dataparser.provider.dat.npcgrp;

import org.dataparser.provider.BasicDataParser;

public class NpcGrpParser extends BasicDataParser<NpcGrpData> {
    @Override
    protected NpcGrpData createData() {
        return new NpcGrpData();
    }

    @Override
    protected String dataFilePath() {
        return "data/dat/Npcgrp_ru.txt";
    }

    @Override
    public boolean canParse(String definition) {
        return definition.startsWith("npc_begin");
    }
}
