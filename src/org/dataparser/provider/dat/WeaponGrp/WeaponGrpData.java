package org.dataparser.provider.dat.WeaponGrp;

public class WeaponGrpData {
    public String tag;
    public String object_id;
    public String drop_type;
    public String drop_anim_type;
    public String drop_radius;
    public String drop_height;
    public String drop_texture;
    public String icon;
    public String durability;
    public String weight;
    public String material_type;
    public String crystallizable;
    public String related_quest_id;
    public String color;
    public String is_attribution;
    public String property_params;
    public String icon_panel;
    public String complete_item_dropsound_type;
    public String inventory_type;
    public String body_part;
    public String handness;
    public String wp_mesh;
    public String texture;
    public String item_sound;
    public String drop_sound;
    public String equip_sound;
    public String effect;
    public String random_damage;
    public String weapon_type;
    public String crystal_type;
    public String mp_consume;
    public String soulshot_count;
    public String spiritshot_count;
    public String curvature;
    public String UNK_10;
    public String can_equip_hero;
    public String is_magic_weapon;
    public String ertheia_fist_scale;
    public String junk;
    public String Enchanted;
    public String variation_effect_type;
    public String variation_icon;
    public String ensoul_slot_count;
    public String is_ensoul;

    public String getObjectId() {
        return object_id;
    }
}
