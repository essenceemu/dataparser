package org.dataparser.provider.dat.ItemBaseInfo;

public class ItemBaseInfoData {
    public String id;
    public String default_price;
    public String is_locked;

    public String getId() {
        return id;
    }
}
