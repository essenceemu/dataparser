package org.dataparser;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;
import org.dataparser.provider.dat.ClientDatProvider;
import org.dataparser.provider.dat.ItemBaseInfo.ItemBaseInfoData;
import org.dataparser.provider.dat.ItemName.ItemNameData;
import org.dataparser.provider.dat.SkillGrp.SkillGrpData;
import org.dataparser.provider.dat.SkillName.SkillNameData;
import org.dataparser.provider.dat.WeaponGrp.WeaponGrpData;
import org.dataparser.provider.dat.npcgrp.NpcGrpData;
import org.dataparser.provider.dat.npcname.NpcNameData;
import org.dataparser.provider.datapack.L2jOrgDataTransformer;
import org.dataparser.provider.l2central.L2CentralProvider;
import org.dataparser.provider.l2central.classes.ClassInfo;
import org.dataparser.provider.l2central.classes.RaceInfo;
import org.dataparser.provider.l2central.classes.SkillInfo;
import org.dataparser.provider.l2central.skillinfo.CachedCentralSkillInfoData;
import org.dataparser.provider.pts.itemdata.ItemData;
import org.dataparser.provider.pts.PTSScriptProvider;
import org.dataparser.provider.pts.npcdata.NpcData;
import org.dataparser.template.Environment;
import org.dataparser.template.Processor;
import org.dataparser.template.Template;
import org.dataparser.template.tokens.Token;
import org.dataparser.template.tokens.Variable;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class MainWindowController {
    @FXML
    private TableView<NpcNameData> npcNamesTable;
    @FXML
    private TableColumn<NpcNameData, String> npcDataIdColumn;
    @FXML
    private TableColumn<NpcNameData, String> npcDataNameColumn;
    @FXML
    private Label npcDataEnvPreviewLabel;
    @FXML
    private TextArea npcDataTemplateEditorArea;
    @FXML
    private Label npcDataTemplateResultLabel;

    @FXML
    private TableView<ItemNameData> itemNamesTable;
    @FXML
    private TableColumn<ItemNameData, String> itemDataIdColumn;
    @FXML
    private TableColumn<ItemNameData, String> itemDataNameColumn;
    @FXML
    private Label itemDataEnvPreviewLabel;
    @FXML
    private TextArea itemDataTemplateEditorArea;
    @FXML
    private Label itemDataTemplateResultLabel;

    @FXML
    private TableView<SkillInfo> classTreeSkillInfoTable;
    @FXML
    private TableColumn<SkillInfo, String> classTreeSkillIdColumn;
    @FXML
    private TableColumn<SkillInfo, String> classTreeSkillNameColumn;
    @FXML
    private TableColumn<SkillInfo, String> classTreeSkillLevelColumn;
    @FXML
    private TableColumn<SkillInfo, String> classTreeSkillMagicLevelColumn;
    @FXML
    private TableColumn<SkillInfo, String> classTreeSkillTypeColumn;
    @FXML
    private TableColumn<SkillInfo, String> classTreeSkillMPColumn;
    @FXML
    private TableColumn<SkillInfo, String> classTreeSkillHPColumn;
    @FXML
    private TableColumn<SkillInfo, String> classTreeSkillSPColumn;
    @FXML
    private TableColumn<SkillInfo, String> classTreeSkillRangeColumn;
    @FXML
    private TableColumn<SkillInfo, String> classTreeSkillAttrColumn;
    @FXML
    private TableColumn<SkillInfo, String> classTreeSkillDescColumn;
    @FXML
    private ComboBox<RaceInfo> classTreeSkillRaceComboBox;
    @FXML
    private ComboBox<ClassInfo> classTreeSkillClassComboBox;

    @FXML
    private TableView<SkillNameData> skillDataTable;
    @FXML
    private TableColumn<SkillNameData, String> skillDataIdColumn;
    @FXML
    private TableColumn<SkillNameData, String> skillDataNameColumn;
    @FXML
    private TableColumn<SkillNameData, String> skillDataDescParamColumn;
    @FXML
    private TableColumn<SkillNameData, String> skillDataDescColumn;
    @FXML
    private Label skillDataEnvPreviewLabel;
    @FXML
    private TextArea skillDataTemplateEditorArea;
    @FXML
    private Label skillDataTemplateResultLabel;

    private Template itemDataTemplate;
    private Template skillDataTemplate;

    @FXML
    private void initialize() {
//        initializeNpcData();
//        initializeItemData();
        initializeClassTrees();
//        initializeSkillData();
    }

    private void initializeNpcData() {
        npcDataIdColumn.setCellValueFactory(new FieldValueFactory<>("id"));
        npcDataNameColumn.setCellValueFactory(new FieldValueFactory<>("name"));

        npcNamesTable.setItems(FXCollections.observableArrayList(ClientDatProvider.getInstance().getNpcNames()));
        npcNamesTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<NpcNameData>() {
            @Override
            public void changed(ObservableValue<? extends NpcNameData> observable, NpcNameData oldValue, NpcNameData newValue) {
                updateNpcDataSelection(newValue);
            }
        });

        npcNamesTable.getSelectionModel().select(0);
    }

    private void initializeItemData() {
        itemDataIdColumn.setCellValueFactory(new FieldValueFactory<>("id"));
        itemDataNameColumn.setCellValueFactory(new FieldValueFactory<>("name"));

        itemDataTemplate = new Template(readString("data/templates/item.template"));

        itemNamesTable.setItems(FXCollections.observableArrayList(ClientDatProvider.getInstance().getItemNames()));
        itemNamesTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ItemNameData>() {
            @Override
            public void changed(ObservableValue<? extends ItemNameData> observable, ItemNameData oldValue, ItemNameData newValue) {
                updateItemDataSelection(newValue);
            }
        });

        itemNamesTable.getSelectionModel().select(0);
    }

    private void initializeClassTrees() {
        classTreeSkillIdColumn.setCellValueFactory(new FieldValueFactory<>("id"));
        classTreeSkillNameColumn.setCellValueFactory(new FieldValueFactory<>("name"));
        classTreeSkillLevelColumn.setCellValueFactory(new FieldValueFactory<>("level"));
        classTreeSkillMagicLevelColumn.setCellValueFactory(new FieldValueFactory<>("magicLevel"));
        classTreeSkillTypeColumn.setCellValueFactory(new FieldValueFactory<>("type"));
        classTreeSkillMPColumn.setCellValueFactory(new FieldValueFactory<>("mp"));
        classTreeSkillHPColumn.setCellValueFactory(new FieldValueFactory<>("hp"));
        classTreeSkillSPColumn.setCellValueFactory(new FieldValueFactory<>("spCost"));
        classTreeSkillRangeColumn.setCellValueFactory(new FieldValueFactory<>("range"));
        classTreeSkillAttrColumn.setCellValueFactory(new FieldValueFactory<>("attr"));
        classTreeSkillDescColumn.setCellValueFactory(new FieldValueFactory<>("description"));

        classTreeSkillRaceComboBox.setItems(FXCollections.observableArrayList(L2CentralProvider.getInstance().getRaceInfos()));
        classTreeSkillRaceComboBox.setCellFactory(new Callback<ListView<RaceInfo>, ListCell<RaceInfo>>() {
            @Override
            public ListCell<RaceInfo> call(ListView<RaceInfo> param) {
                return new ListCell<RaceInfo>(){
                    @Override
                    protected void updateItem(RaceInfo info, boolean empty) {
                        super.updateItem(info, empty);

                        if(info != null){
                            setText(info.getName());
                        }else{
                            setText(null);
                        }
                    }

                };
            }
        });

        classTreeSkillRaceComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<RaceInfo>() {
            @Override
            public void changed(ObservableValue<? extends RaceInfo> observable, RaceInfo oldValue, RaceInfo newValue) {
                classTreeSkillClassComboBox.setItems(FXCollections.observableArrayList(newValue.getClassInfos()));
            }
        });

        classTreeSkillClassComboBox.setCellFactory(new Callback<ListView<ClassInfo>, ListCell<ClassInfo>>() {
            @Override
            public ListCell<ClassInfo> call(ListView<ClassInfo> param) {
                return new ListCell<ClassInfo>(){
                    @Override
                    protected void updateItem(ClassInfo info, boolean empty) {
                        super.updateItem(info, empty);

                        if(info != null){
                            setText(info.getName());
                        }else{
                            setText(null);
                        }
                    }

                };
            }
        });

        classTreeSkillClassComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ClassInfo>() {
            @Override
            public void changed(ObservableValue<? extends ClassInfo> observable, ClassInfo oldValue, ClassInfo newValue) {
                classTreeSkillInfoTable.setItems(FXCollections.observableArrayList(L2CentralProvider.getInstance().getOrLoadClassInfo(newValue).getSkillInfoList()));
            }
        });
    }

    private void initializeSkillData() {
        skillDataIdColumn.setCellValueFactory(new PropertyValueFactory<>("skillId"));
        skillDataNameColumn.setCellValueFactory(new FieldValueFactory<>("name"));
        skillDataDescParamColumn.setCellValueFactory(new FieldValueFactory<>("desc_param"));
        skillDataDescColumn.setCellValueFactory(new FieldValueFactory<>("desc"));

        skillDataTemplate = new Template(readString("data/templates/skill.template"));

        skillDataTable.setItems(FXCollections.observableArrayList(ClientDatProvider.getInstance().getSkillNames()));
        skillDataTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<SkillNameData>() {
            @Override
            public void changed(ObservableValue<? extends SkillNameData> observable, SkillNameData oldValue, SkillNameData newValue) {
                updateSkillDataSelection(newValue);
            }
        });

        skillDataTable.getSelectionModel().select(0);
    }

    private void updateNpcDataSelection(NpcNameData data) {
        //List<Token> tokens = Processor.getTemplateTokens(itemDataTemplate);
        //List<Variable> variableTokens = Processor.getVariableTokens(tokens);

        Environment env = new Environment();
        env.setVariablesFromClassFields(data, "dat.");

        NpcNameData npcNameData = ClientDatProvider.getInstance().getNpcName(data.id);
        if (npcNameData != null) {
            env.setVariablesFromClassFields(npcNameData, "dat.");
        }

        NpcGrpData npcGrpData = ClientDatProvider.getInstance().getNpcGrp(data.id);
        if (npcGrpData != null) {
            env.setVariablesFromClassFields(npcGrpData, "dat.");
        }

        NpcData npcData = PTSScriptProvider.getInstance().getNpcData(data.id);
        if (npcData != null) {
            env.setVariablesFromClassFields(npcData, "pts.");
        }

        L2jOrgDataTransformer.updateItemDataEnvironment(env);

        //Processor.updateVariableTokens(variableTokens, env);
        //String templateResult = Processor.generateTemplate(tokens);

        StringBuilder builder = new StringBuilder(1024);
        List<String> variables = new ArrayList(env.getVariables().keySet());
        Collections.sort(variables);
        for (String variableName : variables) {
            builder.append(variableName).append(":").append(env.getVariables().get(variableName)).append("\n");
        }

        npcDataEnvPreviewLabel.setText(builder.toString());
        //npcDataTemplateEditorArea.setText(itemDataTemplate.getTemplate());
        //npcDataTemplateResultLabel.setText(templateResult);
    }

    private void updateItemDataSelection(ItemNameData data) {
        List<Token> tokens = Processor.getTemplateTokens(itemDataTemplate);
        List<Variable> variableTokens = Processor.getVariableTokens(tokens);

        Environment env = new Environment();
        env.setVariablesFromClassFields(data, "dat.");

        ItemBaseInfoData itemBaseInfoData = ClientDatProvider.getInstance().getItemBaseInfo(data.id);
        if (itemBaseInfoData != null) {
            env.setVariablesFromClassFields(itemBaseInfoData, "dat.");
        }

        WeaponGrpData weaponGrpData = ClientDatProvider.getInstance().getWeaponGrp(data.id);
        if (weaponGrpData != null) {
            env.setVariablesFromClassFields(weaponGrpData, "dat.");
        }

        ItemData itemData = PTSScriptProvider.getInstance().getItem(data.id);
        if (itemData != null) {
            env.setVariablesFromClassFields(itemData, "pts.");
        }

        L2jOrgDataTransformer.updateItemDataEnvironment(env);

        Processor.updateVariableTokens(variableTokens, env);
        String templateResult = Processor.generateTemplate(tokens);

        StringBuilder builder = new StringBuilder(1024);
        List<String> variables = new ArrayList(env.getVariables().keySet());
        Collections.sort(variables);
        for (String variableName : variables) {
            builder.append(variableName).append(":").append(env.getVariables().get(variableName)).append("\n");
        }

        itemDataEnvPreviewLabel.setText(builder.toString());
        itemDataTemplateEditorArea.setText(itemDataTemplate.getTemplate());
        itemDataTemplateResultLabel.setText(templateResult);
    }

    private void updateSkillDataSelection(SkillNameData data) {
        List<Token> tokens = Processor.getTemplateTokens(skillDataTemplate);
        List<Variable> variableTokens = Processor.getVariableTokens(tokens);

        Environment env = new Environment();
        env.setVariablesFromClassFields(data, "dat.");

        SkillGrpData skillGrpData = ClientDatProvider.getInstance().getSkillGrp(data.getSkillId());
        if (skillGrpData != null) {
            env.setVariablesFromClassFields(skillGrpData, "dat.");
        }

        CachedCentralSkillInfoData cachedSkillInfo = L2CentralProvider.getInstance().getCachedSkillInfo(data.getSkillId());
        if (cachedSkillInfo != null) {
            env.setVariablesFromClassFields(cachedSkillInfo, "l2central.");
        }

//        L2jOrgDataTransformer.updateEnvironment(env);

        Processor.updateVariableTokens(variableTokens, env);
        String templateResult = Processor.generateTemplate(tokens);

        StringBuilder builder = new StringBuilder(1024);
        List<String> variables = new ArrayList(env.getVariables().keySet());
        Collections.sort(variables);
        for (String variableName : variables) {
            builder.append(variableName).append(":").append(env.getVariables().get(variableName)).append("\n");
        }

        skillDataEnvPreviewLabel.setText(builder.toString());
        skillDataTemplateEditorArea.setText(skillDataTemplate.getTemplate());
        skillDataTemplateResultLabel.setText(templateResult);
    }

    private String readString(String path) {
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            return new String(encoded, StandardCharsets.UTF_8);
        } catch (IOException e) {
            System.out.println(e.toString());
        }

        return null;
    }
}
