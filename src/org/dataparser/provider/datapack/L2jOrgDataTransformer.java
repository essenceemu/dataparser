package org.dataparser.provider.datapack;

import org.dataparser.template.Environment;

public class L2jOrgDataTransformer {
    public static void updateItemDataEnvironment(Environment env) {
        if (env.getVariableValue("%dat.ItemNameData.description%") != null) {
            String value = env.getVariableValue("%dat.ItemNameData.description%");
            if (!value.equalsIgnoreCase("[]"))
                env.setVariable("%L2jOrg.ItemNameData.description%", value);
        }

        if (env.getVariableValue("%pts.ItemData.type%") != null) {
            String value = env.getVariableValue("%pts.ItemData.type%");
            String newValue = null;
            if (value.equalsIgnoreCase("weapon")) {
                newValue = "Weapon";
            } else if (value.equalsIgnoreCase("armor") || value.equalsIgnoreCase("accessary")) {
                newValue = "Armor";
            }
            if (value.equalsIgnoreCase("etcitem") || value.equalsIgnoreCase("asset") || value.equalsIgnoreCase("questitem")) {
                newValue = "EtcItem";
            }

            env.setVariable("%L2jOrg.ItemData.type%", newValue);
        }

        if (env.getVariableValue("%dat.WeaponGrpData.icon%") != null) {
            String value = env.getVariableValue("%dat.WeaponGrpData.icon%");
            int endOfFirstBlock = value.indexOf(";");
            if (endOfFirstBlock > 2) {
                String newValue = value.substring(2, endOfFirstBlock - 1);
                env.setVariable("%L2jOrg.WeaponGrpData.icon%", newValue);
            }
        }

        if (env.getVariableValue("%dat.ItemNameData.default_action%") != null) {
            String value = env.getVariableValue("%dat.ItemNameData.default_action%");
            String newValue = value.replace("[", "").replace("]", "").replace("action_", "").toUpperCase();
            env.setVariable("%L2jOrg.ItemNameData.default_action%", newValue);
        }

        if (env.getVariableValue("%pts.ItemData.weapon_type%") != null) {
            String value = env.getVariableValue("%pts.ItemData.weapon_type%");
            String newValue = value.toUpperCase();
            if (!newValue.equalsIgnoreCase("NONE"));
                env.setVariable("%L2jOrg.ItemData.weapon_type%", value.toUpperCase());
        }

        if (env.getVariableValue("%pts.ItemData.slot_bit_type%") != null) {
            String value = env.getVariableValue("%pts.ItemData.slot_bit_type%");
            String newValue = value.replace("{", "").replace("}", "");
            if (!newValue.equalsIgnoreCase("none")) {
                env.setVariable("%L2jOrg.ItemData.slot_bit_type", newValue);
            }
        }

        if (env.getVariableValue("%pts.ItemData.immediate_effect%") != null) {
            env.setVariable("%L2jOrg.ItemData.immediate_effect%", Boolean.valueOf(env.getVariableValue("%pts.ItemData.immediate_effect%")).toString());
        }

        if (env.getVariableValue("%pts.ItemData.material_type%") != null) {
            env.setVariable("%L2jOrg.ItemData.material_type%", env.getVariableValue("%pts.ItemData.material_type%").toUpperCase());
        }

        if (env.getVariableValue("%pts.ItemData.validate_params%") != null) {
            //%pts.ItemData.validate_params%:{{pattack;5};{pcritical;12};{phit;-3.75};{pavoid;0};{shielddefense;0};{shielddefenserate;0};{pattackspeed;433};{mattack;5};{pdefense;0};{mdefense;0}}
            String value = env.getVariableValue("%pts.ItemData.validate_params%");

            String pattack = getValidateParamValue(value, "pattack");
            String pcritical = getValidateParamValue(value, "pcritical");
            String phit = getValidateParamValue(value, "phit");
            String pavoid = getValidateParamValue(value, "pavoid");
            String shielddefense = getValidateParamValue(value, "shielddefense");
            String shielddefenserate = getValidateParamValue(value, "shielddefenserate");
            String pattackspeed = getValidateParamValue(value, "pattackspeed");
            String mattack = getValidateParamValue(value, "mattack");
            String pdefense = getValidateParamValue(value, "pdefense");
            String mdefense = getValidateParamValue(value, "mdefense");

            StringBuilder statsBuilder = new StringBuilder("<stats>\n");
            int startLength = statsBuilder.length();
            if (pattack != null && !pattack.equals("0")) {
                statsBuilder.append("        <stat type=\"pAtk\">").append(pattack).append("</stat>\n");
            }
            if (mattack != null && !mattack.equals("0")) {
                statsBuilder.append("        <stat type=\"mAtk\">").append(mattack).append("</stat>\n");
            }
            if (pcritical != null && !pcritical.equals("0")) {
                statsBuilder.append("        <stat type=\"rCrit\">").append(pcritical).append("</stat>\n");
            }
            if (pattackspeed != null && !pattackspeed.equals("0")) {
                statsBuilder.append("        <stat type=\"pAtkSpd\">").append(pattackspeed).append("</stat>\n");
            }
            if (pdefense != null && !pdefense.equals("0")) {
                statsBuilder.append("        <stat type=\"pDef\">").append(pdefense).append("</stat>\n");
            }
            if (mdefense != null && !mdefense.equals("0")) {
                statsBuilder.append("        <stat type=\"mDef\">").append(mdefense).append("</stat>\n");
            }
            if (shielddefense != null && !shielddefense.equals("0")) {
                statsBuilder.append("        <stat type=\"sDef\">").append(shielddefense).append("</stat>\n");
            }

            if (env.getVariableValue("%pts.ItemData.attack_range%") != null && !env.getVariableValue("%pts.ItemData.attack_range%").equals("0")) {
                statsBuilder.append("        <stat type=\"pAtkRange\">").append(env.getVariableValue("%pts.ItemData.attack_range%")).append("</stat>\n");
            }
            if (env.getVariableValue("%dat.WeaponGrpData.random_damage%") != null && !env.getVariableValue("%dat.WeaponGrpData.random_damage%").equals("0")) {
                statsBuilder.append("        <stat type=\"randomDamage\">").append(env.getVariableValue("%dat.WeaponGrpData.random_damage%")).append("</stat>\n");
            }

            if (startLength != statsBuilder.length()) {
                statsBuilder.append("    </stats>");
                env.setVariable("%L2jOrg.Item.StatsBlock%", statsBuilder.toString());
            }
        }
    }

    private static String getValidateParamValue(String paramString, String paramName) {
        paramName = paramName + ";";
        int start = paramString.indexOf(paramName);
        if (start < 0)
            return null;

        start += paramName.length();
        int end = paramString.indexOf("}", start);
        if (end < 0)
            return null;

        return paramString.substring(start, end);
    }
}
