package org.dataparser.provider.l2central.classes;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ClassesSkillsParser {
    static String baseUrl = "https://l2central.info";
    Map<String, RaceInfo> raceInfoList;

    public void load() {
        try {
            Document doc = Jsoup.connect(baseUrl + "/classic/Люди").get();
            Elements classTableElements = doc.select("#tabber-26aef0aaa9905c0812da1424f1f75f27");
            Elements raceElements = classTableElements.select("div[class=\"tabbertab\"]");
            raceInfoList = new HashMap<>();
            for (Element race : raceElements) {
                String name = race.attr("title");
                RaceInfo raceInfo = new RaceInfo(name);
                Elements classElements = race.select("a[href]");
                for (Element classElement : classElements) {
                    raceInfo.addClass(new ClassInfo(classElement.html(), baseUrl + classElement.attr("href")));
                }

                raceInfoList.put(raceInfo.getName(), raceInfo);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        dumpAllSkillInfoToFileIfNeeded();
    }

    public RaceInfo getRace(String name) {
        return raceInfoList.get(name);
    }

    public Collection<RaceInfo> getRaceInfoList() {
        return raceInfoList.values();
    }

    public ClassInfo getOrLoadClassInfo(ClassInfo classInfo) {
        if (!classInfo.skillInfoList.isEmpty()) {
            return classInfo;
        }

        try {
            Document classInfoDoc = Jsoup.connect(classInfo.url).get();
            Elements classSkillsByLevels = classInfoDoc.select(".tabber").get(1).select(".tabbertab");
            for (Element skillsByLevel : classSkillsByLevels) {
                Elements skillsElements = skillsByLevel.select("tr[align=center]");
                for (Element skillElement : skillsElements) {
                    String skillId = null;
                    String iconUrl = null;
                    Element iconElement = skillElement.select("img[alt]").first();
                    if (iconElement != null) {
                        String iconImageValue = iconElement.attr("alt");
                        String[] iconValues = iconImageValue.split(" ");
                        skillId = iconValues[1];
                        iconUrl = baseUrl + iconElement.attr("src");
                    }
                    else {
                        Element missingIconHref = skillElement.select("a[title]").first();
                        String title = missingIconHref.attr("title");
                        if (title != null)
                            skillId = title.split(" ")[1];
                    }

                    if (skillId == null) {
                        System.out.println("Can't parse id for skill at url " + classInfo.url);
                        continue;
                    }

                    SkillInfo skillInfo = new SkillInfo();
                    skillInfo.id = skillId;
                    skillInfo.icon = iconUrl;
                    skillInfo.name = skillElement.child(1).child(0).attr("title");
                    skillInfo.magicLevel = skillsByLevel.attr("title");
                    skillInfo.level = skillElement.child(2).html();
                    skillInfo.type = skillElement.child(3).html();
                    skillInfo.mp = skillElement.child(4).child(0).html();
                    skillInfo.hp = skillElement.child(5).child(0).html();
                    skillInfo.spCost = skillElement.child(6).html();
                    skillInfo.range = skillElement.child(7).html();
                    skillInfo.attr = skillElement.child(8).html();
                    skillInfo.description = skillElement.child(9).html();

                    classInfo.addSkillInfo(skillInfo);
                }
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }

        return classInfo;
    }

    private void dumpAllSkillInfoToFileIfNeeded() {
        try {
            File newDataFile = new File("data/l2central/SkillInfoData.txt");
            if (newDataFile.exists()) {
                return;
            }

            newDataFile.delete();
            newDataFile.createNewFile();
            FileWriter fw = new FileWriter(newDataFile);

            StringBuilder builder = new StringBuilder(1024);
            Field[] fields = SkillInfo.class.getFields();

            for (RaceInfo raceInfo : raceInfoList.values()) {
                for (ClassInfo classInfo : raceInfo.getClassInfos()) {
                    getOrLoadClassInfo(classInfo);

                    String prefix = "skillinfo_begin\trace_name=" + raceInfo.getName() + "\tclass_name=" + classInfo.getName();
                    builder.append(prefix);
                    for (SkillInfo info : classInfo.getSkillInfoList()) {
                        for (Field field : fields) {
                            try {
                                builder.append("\t").append(field.getName()).append("=").append(field.get(info));
                            }
                            catch (IllegalAccessException e) {
                                System.out.println(e.toString());
                            }
                        }

                        builder.append("\tskillinfo_end\n");
                        fw.write(builder.toString());
                        builder.setLength(prefix.length());
                    }
                    fw.flush();
                    builder.setLength(0);
                }
            }
            fw.close();

        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
}
