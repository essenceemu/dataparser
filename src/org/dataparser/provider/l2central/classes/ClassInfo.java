package org.dataparser.provider.l2central.classes;

import java.util.ArrayList;
import java.util.List;

public class ClassInfo {
    String name;
    String url;
    List<SkillInfo> skillInfoList = new ArrayList<>();

    public ClassInfo(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void addSkillInfo(SkillInfo skillInfo) {
        skillInfoList.add(skillInfo);
    }

    public List<SkillInfo> getSkillInfoList() {
        return skillInfoList;
    }
}
