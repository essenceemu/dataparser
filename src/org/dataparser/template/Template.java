package org.dataparser.template;

public class Template {
    private String template;

    public Template(String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template;
    }
}
