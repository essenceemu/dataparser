package org.dataparser.provider.l2central.npc;

public class NpcItemDropInfo {
    public String id;
    public String minAmount;
    public String maxAmount;
    public String minChance;
    public String maxChance;
}
