package org.dataparser.provider.dat.WeaponGrp;

import org.dataparser.provider.BasicDataParser;

public class WeaponGrpParser extends BasicDataParser<WeaponGrpData> {
    @Override
    protected WeaponGrpData createData() {
        return new WeaponGrpData();
    }

    @Override
    protected String dataFilePath() {
        return "data/dat/Weapongrp.txt";
    }

    @Override
    public boolean canParse(String definition) {
        return definition.startsWith("item_begin");
    }
}
