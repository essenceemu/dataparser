package org.dataparser.provider.dat;

import org.dataparser.provider.dat.ItemBaseInfo.ItemBaseInfoData;
import org.dataparser.provider.dat.ItemBaseInfo.ItemBaseInfoParser;
import org.dataparser.provider.dat.ItemName.ItemNameData;
import org.dataparser.provider.dat.ItemName.ItemNameParser;
import org.dataparser.provider.dat.SkillGrp.SkillGrpData;
import org.dataparser.provider.dat.SkillGrp.SkillGrpParser;
import org.dataparser.provider.dat.SkillName.SkillNameData;
import org.dataparser.provider.dat.SkillName.SkillNameParser;
import org.dataparser.provider.dat.WeaponGrp.WeaponGrpData;
import org.dataparser.provider.dat.WeaponGrp.WeaponGrpParser;
import org.dataparser.provider.dat.npcgrp.NpcGrpData;
import org.dataparser.provider.dat.npcgrp.NpcGrpParser;
import org.dataparser.provider.dat.npcname.NpcNameData;
import org.dataparser.provider.dat.npcname.NpcNameParser;

import java.util.*;
import java.util.stream.Collectors;

public class ClientDatProvider {
    private Map<String, ItemNameData> itemNamesDataMap = new HashMap<>();
    private Map<String, ItemBaseInfoData> itemBaseInfosDataMap = new HashMap<>();
    private Map<String, WeaponGrpData> weaponGrpsDataMap = new HashMap<>();
    private Map<String, SkillGrpData> skillGrpDataMap = new HashMap<>();
    private Map<String, SkillNameData> skillNameDataMap = new HashMap<>();
    private Map<String, NpcNameData> npcNamesDataMap = new HashMap<>();
    private Map<String, NpcGrpData> npcGrpDataMap = new HashMap<>();

    private ClientDatProvider() {
        load();
    }

    private void load() {
        try {
            itemNamesDataMap = new ItemNameParser().parse().stream().collect(Collectors.toMap(ItemNameData::getId, item -> item));
            itemBaseInfosDataMap = new ItemBaseInfoParser().parse().stream().collect(Collectors.toMap(ItemBaseInfoData::getId, item -> item));
            weaponGrpsDataMap = new WeaponGrpParser().parse().stream().collect(Collectors.toMap(WeaponGrpData::getObjectId, item -> item));
            skillGrpDataMap = new SkillGrpParser().parse().stream().collect(Collectors.toMap(SkillGrpData::getSkillId, skill -> skill));
            npcNamesDataMap = new NpcNameParser().parse().stream().collect(Collectors.toMap(NpcNameData::getId, name -> name));
            npcGrpDataMap = new NpcGrpParser().parse().stream().collect(Collectors.toMap(NpcGrpData::getId, data -> data));

            List<SkillNameData> skillNameDataList = new SkillNameParser().parse();
            skillNameDataMap = skillNameDataList.stream().collect(Collectors.toMap(SkillNameData::getSkillId, skill -> skill));
            postProcessSkillNames(skillNameDataList);
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public ItemNameData getItemName(String id) {
        return itemNamesDataMap.get(id);
    }
    public Collection<ItemNameData> getItemNames() {
        return itemNamesDataMap.values();
    }

    public ItemBaseInfoData getItemBaseInfo(String id) {
        return itemBaseInfosDataMap.get(id);
    }
    public Collection<ItemBaseInfoData> getItemBaseInfos() {
        return itemBaseInfosDataMap.values();
    }

    public WeaponGrpData getWeaponGrp(String id) {
        return weaponGrpsDataMap.get(id);
    }
    public Collection<WeaponGrpData> getWeaponGrps() {
        return weaponGrpsDataMap.values();
    }

    public SkillGrpData getSkillGrp(String id) {
        return skillGrpDataMap.get(id);
    }
    public Collection<SkillGrpData> getSkillGrps() {
        return skillGrpDataMap.values();
    }

    public SkillNameData getSkillName(String id) {
        return skillNameDataMap.get(id);
    }
    public Collection<SkillNameData> getSkillNames() {
        return skillNameDataMap.values();
    }

    public Collection<NpcNameData> getNpcNames() {
        return npcNamesDataMap.values();
    }
    public NpcNameData getNpcName(String id) {
        return npcNamesDataMap.get(id);
    }

    public Collection<NpcGrpData> getNpcGrpDatas() {
        return npcGrpDataMap.values();
    }
    public NpcGrpData getNpcGrp(String id) {
        return npcGrpDataMap.get(id);
    }

    private void postProcessSkillNames(List<SkillNameData> skillNameDataList) {
        Map<String, String> skillDescriptions = new HashMap<>();
        for (SkillNameData skillNameData : skillNameDataList) {
            if (skillNameData.desc_param.isEmpty() || skillNameData.desc_param.equalsIgnoreCase("[]")) {
                continue;
            }

            if (skillNameData.desc == null || skillNameData.desc.isEmpty() || skillNameData.desc.equalsIgnoreCase("[]")) {
                String parentSkillId = skillNameData.getParentSkillId();
                String cachedDesc = skillDescriptions.get(parentSkillId);
                if (cachedDesc != null) {
                    skillNameData.desc = cachedDesc;
                }
                else {
                    SkillNameData parentSkill = skillNameDataMap.get(skillNameData.getParentSkillId());
                    if (parentSkill == null || parentSkill.desc == null || parentSkill.desc.isEmpty()) {
                        continue;
                    }

                    skillNameData.desc = parentSkill.desc;
                }
            }
            else {
                skillDescriptions.put(skillNameData.getSkillId(), skillNameData.desc);
            }

            String[] params = skillNameData.desc_param.substring(1, skillNameData.desc_param.length() - 1).split(";");
            if (params.length == 0) {
                continue;
            }

            for (int i = 0; i < params.length; ++i) {
                skillNameData.desc = skillNameData.desc.replace("$s" + (i+1), params[i]);
            }
        }
    }

    public static ClientDatProvider getInstance() {
        return Singleton.INSTANCE;
    }

    private static class Singleton {
        private static final ClientDatProvider INSTANCE = new ClientDatProvider();
    }
}
