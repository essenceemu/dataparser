 armor_type
 attack_range
 attack_skill
 attack_speed
 attr_groupid
 auction_itemtype
 avoid_modify
 base_attribute_attack
 base_attribute_defend
 blessed
 can_move
 capsuled_items
 category
 consume_type
 create_items
 critical
 critical_attack_skill
 crystal_count
 crystal_item
 crystal_type
 damage_range
 damaged
 default_action
 default_price
 delay_share_group
 drop_period
 dual_fhit_rate
 durability
 duration
 elemental_enable
 enchant_enable
 enchanted
 equip_condition
 equip_reuse_delay
 equip_skill
 etcitem_type
 ex_immediate_effect
 for_npc
 general_itemtype
 hit_modify
 html
 immediate_effect
 initial_count
 is_attribution
 is_destruct
 is_drop
 is_hero_entry
 is_npctrade
 is_olympiad_can_use
 is_pledgegame_can_use
 is_premium
 is_private_store
 is_trade
 item_equip_option
 item_multi_skill_list
 item_skill
 item_skill_enchanted_four
 item_type
 keep_type
 magic_skill
 magic_weapon
 magical_damage
 magical_defense
 material_type
 mp_bonus
 mp_consume
 period
 physical_damage
 physical_defense
 price
 property_params
 random_damage
 recipe_id
 reduced_mp_consume
 reduced_soulshot
 reduced_spiritshot
 reuse_delay
 shape_shiftable
 shield_defense
 shield_defense_rate
 slot_bit_type
 soulshot_count
 spiritshot_count
 unequip_skill
 use_condition
 use_skill_distime
 validate_params
 weapon_type
 weight