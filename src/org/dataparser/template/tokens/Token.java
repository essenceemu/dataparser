package org.dataparser.template.tokens;

import java.util.List;

public class Token {
    private final String value;

    public Token(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public boolean isVariable() {
        return false;
    }

    public boolean isBlock() {
        return false;
    }

    public boolean isEmpty() {
        return value == null;
    }

    public List<Token> getChildren() {
        return null;
    }
}