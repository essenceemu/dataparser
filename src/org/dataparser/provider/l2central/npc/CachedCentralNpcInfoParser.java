package org.dataparser.provider.l2central.npc;

import org.dataparser.provider.BasicDataParser;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CachedCentralNpcInfoParser extends BasicDataParser<NpcBasicInfo> {
    static String dataFilePath = "data/l2central/NpcInfoData.txt";
    @Override
    protected NpcBasicInfo createData() {
        return new NpcBasicInfo();
    }

    @Override
    protected String dataFilePath() {
        return dataFilePath;
    }

    @Override
    public boolean canParse(String definition) {
        return definition.startsWith("npcinfo_begin");
    }

    public static boolean hasCachedData() {
        return Files.exists(Paths.get(dataFilePath));
    }

    @Override
    protected boolean parseProperties(NpcBasicInfo data, String[] properties) {
        boolean result = super.parseProperties(data, properties);
        if (!result) {
            return result;
        }

        for (String property : properties) {
            if (property.startsWith("skills")) {
                data.skills = (List<NpcSkillInfo>)parseCustomDefinition(property, NpcSkillInfo.class);
            }
            else if (property.startsWith("general_drop")) {
                data.generalItems = (List<NpcItemDropInfo>)parseCustomDefinition(property, NpcItemDropInfo.class);
            }
            else if (property.startsWith("other_drop")) {
                data.otherItems = (List<NpcItemDropInfo>)parseCustomDefinition(property, NpcItemDropInfo.class);
            }
            else if (property.startsWith("spoil_drop")) {
                data.spoilItems = (List<NpcItemDropInfo>)parseCustomDefinition(property, NpcItemDropInfo.class);
            }
            else if (property.startsWith("attr")) {
                String[] objectParts = property.split("=");
                String valuePart = objectParts[1];
                if (!valuePart.startsWith("[") || !valuePart.endsWith("]")) {
                    continue;
                }

                String[] objectFieldValues = valuePart.substring(1, valuePart.length() - 1).split(";");

                data.attInfo = new NpcAttributeInfo();
                data.attInfo.attr = NpcAttributeInfo.Attribute.valueOf(objectFieldValues[0]);
                data.attInfo.exp = objectFieldValues[1];
            }
        }

        return true;
    }
}
