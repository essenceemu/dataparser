package org.dataparser.provider.l2central.classes;

import java.util.*;

public class RaceInfo {
    private String name;
    private Map<String, ClassInfo> classInfoList = new HashMap<>();

    public RaceInfo(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addClass(ClassInfo classInfo) {
        classInfoList.put(classInfo.name, classInfo);
    }

    public ClassInfo getClassInfo(String name) {
        return classInfoList.get(name);
    }

    public Collection<ClassInfo> getClassInfos() {
        return classInfoList.values();
    }
}
