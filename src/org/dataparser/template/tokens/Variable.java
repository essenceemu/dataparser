package org.dataparser.template.tokens;

public class Variable extends Token {
    private String newValue;

    public Variable(String name) {
        super(name);
    }

    public String getVariableName() {
        return super.getValue();
    }

    public void setVariableValue(String newValue) {
        this.newValue = newValue;
    }

    public String getValue() {
        return newValue != null ? newValue : super.getValue();
    }

    @Override
    public boolean isEmpty() {
        return newValue == null;
    }

    @Override
    public boolean isVariable() {
        return true;
    }
}
