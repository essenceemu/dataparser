package org.dataparser.provider.l2central.classes;

public class SkillInfo {
    public String id;
    public String icon;
    public String name;
    public String magicLevel;
    public String level;
    public String type;
    public String mp;
    public String hp;
    public String spCost;
    public String range;
    public String attr;
    public String description;
}
