package org.dataparser.provider.pts;

import org.dataparser.provider.pts.itemdata.ItemData;
import org.dataparser.provider.pts.itemdata.ItemDataParser;
import org.dataparser.provider.pts.npcdata.NpcData;
import org.dataparser.provider.pts.npcdata.NpcDataParser;

import java.util.*;
import java.util.stream.Collectors;

public class PTSScriptProvider {
    private Map<String, ItemData> itemDataMap = new HashMap<>();
    private Map<String, NpcData> npcDataMap = new HashMap<>();

    private PTSScriptProvider() {
        load();
    }

    private void load() {
        try {
            itemDataMap = new ItemDataParser().parse().stream().collect(Collectors.toMap(ItemData::getId, item -> item));
            npcDataMap = new NpcDataParser().parse().stream().collect(Collectors.toMap(NpcData::getId, npc -> npc));
        }
        catch (Exception e) {
            System.out.println(e.toString());
        }
    }

    public ItemData getItem(String id) {
return itemDataMap.get(id);
    }
    public Collection<ItemData> getItems() {
        return itemDataMap.values();
    }

    public NpcData getNpcData(String id) {
        return npcDataMap.get(id);
    }
    public Collection<NpcData> getNpcs() {
        return npcDataMap.values();
    }

    public static PTSScriptProvider getInstance() {
        return Singleton.INSTANCE;
    }

    private static class Singleton {
        private static final PTSScriptProvider INSTANCE = new PTSScriptProvider();
    }
}
