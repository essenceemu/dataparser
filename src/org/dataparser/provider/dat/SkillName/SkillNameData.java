package org.dataparser.provider.dat.SkillName;

public class SkillNameData {
    public String skill_id;
    public String skill_level;
    public String skill_sublevel;
    public String name;
    public String desc;
    public String desc_param;
    public String enchant_name;
    public String enchant_name_param;
    public String enchant_desc;
    public String enchant_desc_param;

    String cachedSkillId;

    public String getSkillId() {
        if (cachedSkillId == null) {
            cachedSkillId = skill_id + ":" + skill_level + ":" + skill_sublevel;
        }
        return cachedSkillId;
    }

    public String getParentSkillId() {
        return skill_id + ":1:0";
    }
}
