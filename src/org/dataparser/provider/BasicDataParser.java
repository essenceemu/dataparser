package org.dataparser.provider;

import sun.font.CreatedFontTracker;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public abstract class BasicDataParser<T> {
    protected abstract T createData();
    protected abstract String dataFilePath();

    protected abstract boolean canParse(String definition);
    public List<T> parse() {
        List<T> parseResult = new ArrayList<>();
        try {
            List<String> definitions = Files.readAllLines(Paths.get(dataFilePath()), StandardCharsets.UTF_8);

            for (String definition : definitions) {
                if (!canParse(definition)) {
                    System.out.println("Can't parse definition :\n" + definition);
                    continue;
                }

                T data = parseDefinition(definition);
                if (data == null) {
                    System.out.println("Error while parsing definition :\n" + definition);
                    continue;
                }

                parseResult.add(data);
            }
        } catch (IOException e) {
            System.out.println(e.toString());
            return null;
        }

        return parseResult;
    }

    protected T parseDefinition(String definition) {
        String[] properties = definition.split("\t");
        T data = createData();
        if (!parseProperties(data, properties)) {
            return null;
        }

        return data;
    }

    private class FieldWithName {
        public Field field;
        public String name;

        public FieldWithName(Field field, String name) {
            this.field = field;
            this.name = name;
        }
    }

    protected boolean parseProperties(T data, String[] properties) {
        Field[] fields = data.getClass().getFields();
        List<FieldWithName> fieldsWithNames = Arrays.stream(fields).filter(field -> field.getType() == String.class).map(field -> new FieldWithName(field, field.getName() + "=")).collect(Collectors.toList());

        for (String property : properties) {
            for (FieldWithName fieldWithName : fieldsWithNames) {
                if (property.startsWith(fieldWithName.name)) {

                    String value = property.substring(fieldWithName.name.length());
                    try {
                        fieldWithName.field.set(data, value);
                    } catch (IllegalAccessException e) {
                        System.out.println(e.toString());
                        return false;
                    }
                }
            }
        }

        return true;
    }

    protected String getProperty(String[] properties, String name) {
        for (String property : properties) {
            if (property.startsWith(name + "=")) {
                String value = property.substring(name.length() + 1);
                return value;
            }
        }

        return null;
    }

    protected Object parseCustomDefinition(String objectDefinition, Class dataClass) {
        String[] objectParts = objectDefinition.split("=");
        String valuePart = objectParts[1];
        if (valuePart.startsWith("{") && valuePart.endsWith("}")) {
            return parseCustomSequence(valuePart, dataClass);
        }
        else if (valuePart.startsWith("[") && valuePart.endsWith("]")) {
            String[] objectFieldValues = valuePart.substring(1, valuePart.length() - 1).split(";");
            try {
                return parseCustomObject(objectFieldValues, dataClass.getConstructor().newInstance());
            }
            catch (Exception e) {
                System.out.println(e.toString());
                return null;
            }
        }

        return null;
    }

    protected List<Object> parseCustomSequence(String objectDefinition, Class dataClass) {
        List<Object> dataList = new ArrayList<>();

        String[] sequenceParts = objectDefinition.substring(1, objectDefinition.length() - 1).split("];");
        for (String objectPart : sequenceParts) {
            String[] objectFieldValues = objectPart.replace("[", "").split(";");
            try {
                Object dataObject = dataClass.getConstructor().newInstance();
                if (!parseCustomObject(objectFieldValues, dataObject)) {
                    return null;
                }
                dataList.add(dataObject);
            }
            catch (Exception e) {
                System.out.println(e.toString());
                return null;
            }
        }

        return dataList;
    }

    protected boolean parseCustomObject(String[] objectDefinition, Object data) {
        Field[] fields = data.getClass().getFields();
        List<FieldWithName> fieldsWithNames = Arrays.stream(fields).filter(field -> field.getType() == String.class).map(field -> new FieldWithName(field, field.getName() + "=")).collect(Collectors.toList());

        if (objectDefinition.length != fieldsWithNames.size()) {
            return false;
        }

        for (int i = 0; i < objectDefinition.length; ++i) {
            Field classField = fieldsWithNames.get(i).field;
            String value = objectDefinition[i];

            try {
                classField.set(data, value);
            } catch (IllegalAccessException e) {
                System.out.println(e.toString());
                return false;
            }
        }

        return true;
    }
}
