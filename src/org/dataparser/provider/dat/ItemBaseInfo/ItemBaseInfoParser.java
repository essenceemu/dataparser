package org.dataparser.provider.dat.ItemBaseInfo;

import org.dataparser.provider.BasicDataParser;

public class ItemBaseInfoParser extends BasicDataParser<ItemBaseInfoData> {
    @Override
    protected ItemBaseInfoData createData() {
        return new ItemBaseInfoData();
    }

    @Override
    protected String dataFilePath() {
        return "data/dat/item_baseinfo.txt";
    }

    @Override
    public boolean canParse(String definition) {
        return definition.startsWith("item_baseinfo_begin");
    }
}
